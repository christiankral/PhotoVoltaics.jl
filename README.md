# PhotoVoltaics.jl

Photo voltaics models based on [PhotoVoltaics Modelica library](https://github.com/christiankral/PhotoVoltaics)

## Model equations to be implemented

```
// Temperature dependent voltage
Vt = Modelica.Constants.k * T_heatPort / Q
// Re-calculate reference voltage and current with respect to reference temperature
VRefActual = VRef * (1 + alphaV * (T_heatPort - TRef))
IRefActual = IRef * (1 + alphaI * (T_heatPort - TRef))
// Actual temperature dependent saturation current is determined from reference voltage and current
Ids = IRefActual / (exp(VRefActual / m / Vt) - 1)
// PV equations
Vt = Modelica.Constants.k * T_heatPort / Q
IdsRef=IRef/(exp(VRef/m/VtRef)
i / npModule = Ids * (exp(v / ns / nsModule / m / Vt) - 1)
```
